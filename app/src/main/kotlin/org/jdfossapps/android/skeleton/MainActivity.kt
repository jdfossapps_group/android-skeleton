package org.jdfossapps.android.skeleton

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import org.jdfossapps.android.skeleton.R

class MainActivity : AppCompatActivity() { 
	override fun onCreate(savedInstanceState: Bundle?) { 
		super.onCreate(savedInstanceState) 
		setContentView(R.layout.activity_main) 
	} 
}